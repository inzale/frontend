import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/postServices/post.service';
import { Post } from '../../interfaces/post.interface';
import { CommonModule } from '@angular/common';
import { PostRequest } from '../../interfaces/postRequest.interface';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
  posts: Post[] = [];

  newPost: FormGroup;

  constructor(private formBuilder: FormBuilder,private postService: PostService) { 
    this.newPost = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      userId: [''] 
    });
  }

  ngOnInit() {
    this.loadPosts();
    const token = sessionStorage.getItem('Token');
    if (token) {
      const decodedToken = this.parseJwt(token);
      const userId = decodedToken.userId;
      console.log(decodedToken,' Token ', userId);
      this.newPost.patchValue({ userId: userId });
    }
  }

  parseJwt(token: string): any {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  

  loadPosts() {
    this.postService.getPosts().subscribe({
      next: (response) => {
        this.posts = response.posts;
        console.log(this.posts, 'Posts cargados');
      },
      error: (error) => {
        console.error('Error al cargar los posts:', error);
      }
    });
  }

  createPost() {
    if (this.newPost.valid) {
      this.postService.createPost(this.newPost.value as PostRequest).subscribe({
        next: (post) => {
          this.posts.unshift(post);
          console.log('Post creado', post);
          this.newPost.reset();
        },
        error: (error) => {
          console.error('Error al crear el post:', error);
        }
      });
    }
  }
  

}
