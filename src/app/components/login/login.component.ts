import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../services/authServices/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.login({ email: this.loginForm.value.email, password: this.loginForm.value.password })
      .subscribe({
        next: (response) => {
          if (response?.token) {
            sessionStorage.setItem('Token',response?.token)
            window.location.href = "/Home"
            //Redireccionamiento
          }
        },
        error: (e) => console.error(e)
      });
    }
  }
}
