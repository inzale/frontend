export interface Post {
    id?: number;
    title: string;
    content: string;
    likesCount?: number;
    createdAt?: Date;
}
  