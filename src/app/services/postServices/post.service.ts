import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment.prod';
import { Post } from '../../interfaces/post.interface';
import { ApiResponse } from '../../interfaces/apiResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiUrl = environment.apiUrl+'/api/posts';

  constructor(private http: HttpClient) {}

  getPosts(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.apiUrl,{
      headers: new HttpHeaders({
        'Authorization': "Bearer "+sessionStorage.getItem('Token') || ""
      })
    });
  }

  createPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.apiUrl, post,{
      headers: new HttpHeaders({
        'Authorization': "Bearer "+sessionStorage.getItem('Token') || ""
      })
    });
  }

  updatePost(post: Post): Observable<Post> {
    return this.http.put<Post>(`${this.apiUrl}/${post.id}`, post,{
      headers: new HttpHeaders({
        'Authorization': "Bearer "+sessionStorage.getItem('Token') || ""
      })
    });
  }

  deletePost(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`,{
      headers: new HttpHeaders({
        'Authorization': "Bearer "+sessionStorage.getItem('Token') || ""
      })
    });
  }
}
