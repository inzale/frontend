import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const token = sessionStorage.getItem('Token');
    if (token) {
      return true; // el usuario puede acceder a la ruta
    } else {
      this.router.navigate(['/login']); // redirigir al usuario al login
      return false; // el usuario no puede acceder a la ruta
    }
  }
}
