import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = sessionStorage.getItem('Token');

    if (token) {
      // Clonar la solicitud para añadir el nuevo header.
      const authReq = request.clone({
        headers: request.headers.set('Authorization', `${token}`)
      });

      // Pasar la solicitud clonada en lugar de la original.
      return next.handle(authReq);
    }

    return next.handle(request);
  }
}
