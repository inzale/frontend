import { Post } from "./post.interface";

export interface ApiResponse {
    posts: Post[];
    currentPage: number;
    totalPages: number;
    totalPosts: number;
  }
  