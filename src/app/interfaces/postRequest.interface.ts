 export interface PostRequest {
    title: string;
    content: string;
    userId: string;
}